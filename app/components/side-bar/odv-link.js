import Component from '@ember/component';
import Ember from 'ember';
import { set } from '@ember/object';

export default Component.extend({
  classNameBindings: [ 'isHover'],
  isHover: false,
  tagName: 'li',
  mouseEnter: function() {
    Ember.Logger.info("mouse entered");
    set(this, 'isHover', true)
  },
  mouseLeave: function() {
    Ember.Logger.info("mouse left");
    set(this, 'isHover', false)
  }
});
